#include <iostream>
#include <string>
#include <vector>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Bool.h>

int LowH = 90;
int HighH = 139;

int LowS = 129; 
int HighS = 245;

int LowV = 67;
int HighV = 213;

using namespace cv;
using namespace std;

Point2f image_centre;// the centre of the image
cv_bridge::CvImagePtr convert_image;   //original image container

const double kp=0.01;
const double ki=0.00;
const double kd=0.00;

ros::Publisher bebop_takeoff;	//declare a publisher to send the takeoff message 					 
ros::Publisher bebop_land;	//declare a publisher to send the land message						
ros::Publisher bebop_cmd_vel_pub;	//declare a publisher to send velocity message

std_msgs::Empty take_off,land;	  //takeoff and land variables 
geometry_msgs::Twist cmd_vel_bebop_y, cmd_vel_bebop_z;   //velocity variable

	
void bebop_image_centre(Mat& image)
{
	image_centre.x = image.cols/2; 
	image_centre.y = image.rows/2;
}

void hover()
{
	cmd_vel_bebop_y.linear.x = 0;
	cmd_vel_bebop_y.linear.y = 0;
	cmd_vel_bebop_y.linear.z = 0;
	cmd_vel_bebop_y.angular.z = 0;
	bebop_cmd_vel_pub.publish (cmd_vel_bebop_y);
}

void tracking_IF(Point2f centrePoint)
{
	
	if(centrePoint.x >0 && centrePoint.y>0)	
	{
		if(centrePoint.x < image_centre.x)
		{
			cmd_vel_bebop_y.linear.y = 0.5;
			cout<<" go left"<<endl;
		}
		if(centrePoint.x > image_centre.x)
		{
			cmd_vel_bebop_y.linear.y = -0.5;
			cout<<"go right"<<endl;
		}
		if(centrePoint.y > image_centre.y)
		{
			cmd_vel_bebop_z.linear.z = -0.5;
			cout<<"go down"<<endl;
		}
		if(centrePoint.y < image_centre.y)
		{
			cmd_vel_bebop_z.linear.z = 0.5;
			cout<<"go down"<<endl;
		}

		bebop_cmd_vel_pub.publish(cmd_vel_bebop_y);
		bebop_cmd_vel_pub.publish(cmd_vel_bebop_z);
		
	}
	else
	{
		hover();
	}
}


void tracking_PID(Point2f centrePoint)
{
	if(centrePoint.x> 0 && centrePoint.y >0)
	{
		static double errorI_x = 0;
		static double errorI_y = 0;
		static double errorPre_x = 0;
		static double errorPre_y = 0;

		double error_x = image_centre.x - centrePoint.x;  // calcualted propotional error
		double error_y = image_centre.y - centrePoint.y;
	
		errorI_x = errorI_x + error_x;  // calculated intergal error
		errorI_y = errorI_y + error_y;

		double errorD_x = error_x - errorPre_x; //calculated derivative error
		double errorD_y = error_y - errorPre_y;
	
		errorPre_x = error_x;
		errorPre_y = error_y;
		
		cmd_vel_bebop_y.linear.y = kp*error_x + ki*errorI_x + kd*errorD_x;// pid controller calculate velocity
		cmd_vel_bebop_z.linear.z = kp*error_y + ki*errorI_y + kd*errorD_y;
	
		bebop_cmd_vel_pub.publish(cmd_vel_bebop_y);
		bebop_cmd_vel_pub.publish(cmd_vel_bebop_z);
	}
	else
	{
		hover();
	}
}







